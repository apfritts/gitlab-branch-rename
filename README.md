# Gitlab Branch Rename

This is a simple tool to rename your Gitlab project branches en masse.

Other steps you'll need to take on your own:

* Update your CI configuration that point to old branches
* Update links in your documentation that point to old branches

## Motivation

To support Black Lives Matter and create safer spaces, I wanted to rename all of the default branches in my Gitlab 
account. I wasn't able to find any tools to do that, so I wrote this one. If you're unsure why this is important or 
want to better understand why I wanted to make these changes, see this great [Twitter thread].

## Installation

This gem is designed to be used from the command line and not currently incorporated into your own projects. If you
wish to do that, happy to help make that happen.

Install this with RVM:

    $ rvm @global do gem install gitlab-branch-rename

or install to your system ruby installation:

    $ sudo gem install gitlab-branch-rename

## Usage

```
Usage: gitlab-branch-rename --from <branch to rename> --to <new branch name> [options]

Specific options:
        --from OLD_BRANCH            Branch to rename
        --to NEW_BRANCH              New branch name
        --endpoint ENDPOINT          Gitlab server endpoint. default: Gitlab cloud
        --token-env-var ENV_VAR_NAME Environment variable to pull token out of
        --only-if-default            Rename only if it is the default branch
        --visibility VISIBILITIES    Comma-separated list of project visibilities default: public,internal,private
        --skip-confirm               Skip confirmation before executing rename
        --pretend                    Pretend to perform actions. Overrides confirm option
        --logfile LOGFILE            Logfile destination. Use - for STDOUT
```

## Development

To setup your development environment, run `bundle install` and `make` to run tests.

To release a new version, update the version number in `lib/gitlab_branch_rename/version.rb`, and then run `make push`.

During development, I used these references:
* https://docs.gitlab.com/ce/api/projects.html#edit-project
* https://www.rubydoc.info/gems/gitlab/Gitlab/Client/Projects

## Contributing

Bug reports and pull requests are welcome on [Gitlab]. This project is intended to be a safe, welcoming space for 
collaboration, and contributors are expected to adhere to the [code of conduct].

## License

The gem is available as open source under the terms of the [MIT License].

## Code of Conduct

Everyone interacting in the Gitlab::Branch::Rename project's codebases, issue trackers, chat rooms and mailing lists 
is expected to follow the [code of conduct].

[code of conduct]: https://gitlab.com/apfritts/gitlab-branch-rename/blob/trunk/CODE_OF_CONDUCT.md
[Gitlab]: https://gitlab.com/apfritts/gitlab-branch-rename
[MIT License]: https://opensource.org/licenses/MIT
[Twitter thread]: https://twitter.com/mislav/status/1270388510684598272
