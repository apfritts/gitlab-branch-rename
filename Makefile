build:
	gem build ./gitlab-branch-rename.gemspec

clean:
	-rm gitlab-branch-rename-*.gem

install: build
	gem install ./gitlab-branch-rename-*.gem

push: clean build
	gem push gitlab-branch-rename-*.gem
	open https://rubygems.org/gems/gitlab-branch-rename
