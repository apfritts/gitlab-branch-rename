require_relative 'lib/gitlab_branch_rename/version'

Gem::Specification.new do |spec|
  spec.name = "gitlab-branch-rename"
  spec.version = GitlabBranchRename::VERSION
  spec.authors = ["AP Fritts"]
  spec.email = ["ap@apfritts.com"]

  spec.summary = "Tool to rename your Gitlab project branches en masse"
  spec.description = "To support Black Lives Matter and create safer spaces, I wanted to rename all of the default " +
      "branches in my Gitlab account. I wasn't able to find any tools to do that, so I wrote this one. If you're " +
      "unsure why this is important or want to better understand why I wanted to make these changes, see this " +
      "great {Twitter thread}[https://twitter.com/mislav/status/1270388510684598272]."
  spec.homepage = "https://gitlab.com/apfritts/gitlab-branch-rename"
  spec.license = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.6.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/apfritts/gitlab-branch-rename"

  spec.add_runtime_dependency "colorize", "~> 0.8.1"
  spec.add_runtime_dependency "gitlab", "~> 4.16"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0")
  end
  spec.bindir = "exe"
  spec.executables = "gitlab-branch-rename"
  spec.require_paths = ["lib"]
end
