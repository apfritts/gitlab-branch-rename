require "gitlab_branch_rename/configuration"
require "gitlab_branch_rename/gitlab_configure"
require "gitlab_branch_rename/logger_configure"
require "gitlab_branch_rename/operation"
require "gitlab_branch_rename/version"

module GitlabBranchRename
  def self.launch
    c = Configuration.configure
    logger = LoggerConfigure.configure c
    logger.debug("Gitlab instance configuration:")
    logger.debug("Endpoint:     #{c.endpoint}")
    logger.debug("Token:        #{c.token.nil? ? "set" : "not set"}")
    logger.debug("Project filter criteria:")
    logger.debug("Visibility:   #{c.has_visibility.join(', ')}")
    logger.debug("Operation options:")
    logger.debug("Old branch:   #{c.branch_to_rename}")
    logger.debug("New branch:   #{c.new_branch_name}")
    logger.debug("Skip confirm: #{c.skip_confirm.nil? ? "yes" : "no"}")
    logger.debug("Pretend:      #{c.pretend.nil? ? "yes" : "no"}")
    logger.debug("Log file:     #{c.logfile == STDOUT ? "-" : c.logfile}")
    GitlabConfigure.configure c
    operation = Operation.new c, logger
    operation.execute
    logger.info("For any local repositories, use these commands to update:")
    logger.info("    git fetch `git remote` --prune")
    logger.info("    git checkout #{c.new_branch_name}")
    logger.info("    git remote set-head `git remote` #{c.new_branch_name}")
    logger.info("    git branch -d #{c.branch_to_rename}")
    logger.info("")
    logger.info("Or all as one line:")
    logger.info("    git fetch `git remote` --prune && git checkout #{c.new_branch_name} && git remote set-head `git remote` #{c.new_branch_name} && git branch -d #{c.branch_to_rename}")
    logger.close
  end
end
