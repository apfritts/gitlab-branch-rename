require "colorize"

module GitlabBranchRename
  class LoggerConfigure
    def self.configure(configuration)
      logger = Logger.new(STDOUT)
      logger.formatter = proc do |severity, datetime, progname, msg|
        date_format = datetime.to_datetime.strftime("%Q")
        output = nil
        color = :black
        case severity
        when "DEBUG"
          output = "D #{date_format} #{msg}\n"
          color = :light_black
        when "INFO"
          output = "I #{date_format} #{msg}\n"
          color = :black
        when "WARN"
          output = "W #{date_format} #{msg}\n"
          color = :yellow
        when "ERROR"
          output = "E #{date_format} #{msg}\n"
          color = :red
        else
          output = "? #{date_format} #{msg}\n"
        end
        if configuration.disable_color
          output
        else
          output.colorize(color)
        end
      end
      logger
    end
  end
end