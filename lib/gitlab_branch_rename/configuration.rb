require "optparse"
require "gitlab_branch_rename/version"

module GitlabBranchRename
  class Configuration
    attr_accessor :token, :branch_to_rename, :new_branch_name,
                  :endpoint, :is_default_branch, :has_visibility, :minimum_access_level,
                  :skip_confirm, :pretend, :disable_color, :logfile

    def initialize
      # Required parameters
      self.branch_to_rename = nil
      self.new_branch_name = nil

      # Criteria parameters with defaults
      self.endpoint = "https://gitlab.com/api/v4"
      self.token = ENV["GITLAB_ACCESS_TOKEN"]
      self.is_default_branch = false
      self.has_visibility = %w[private internal public]
      self.minimum_access_level = 40

      # Optional parameters
      self.skip_confirm = false
      self.pretend = false
      self.disable_color = false
      self.logfile = STDOUT
    end

    def self.configure
      configuration = Configuration.new
      optparse = OptionParser.new do |opts|
        opts.banner = "Usage: gitlab-branch-rename --from <branch to rename> --to <new branch name> [options]"
        opts.separator ""
        opts.separator "Specific options:"
        opts.on("--from OLD_BRANCH", "Branch to rename (required)") do |from|
          configuration.branch_to_rename = from
        end
        opts.on("--to NEW_BRANCH", "New branch name (required)") do |to|
          configuration.new_branch_name = to
        end
        opts.on("--endpoint ENDPOINT", "Gitlab server endpoint, default: https://gitlab.com/api/v4") do |endpoint|
          configuration.endpoint = endpoint
        end
        opts.on("--token-env-var ENV_VAR_NAME", "Environment variable to use for Gitlab API token, default: GITLAB_ACCESS_TOKEN") do |token_env_var|
          configuration.token = ENV[token_env_var]
        end
        opts.on("--only-if-default", "Rename only if the old branch is currently the default project branch") do
          configuration.is_default_branch = true
        end
        opts.on("--visibility VISIBILITIES", "Comma-separated list of project visibilities, default: public,internal,private") do |visibility|
          configuration.has_visibility = visibility.split ","
        end
        opts.on("--skip-confirm", "Skip confirmation before executing rename") do
          configuration.skip_confirm = true
        end
        opts.on("--pretend", "Pretend to perform actions. Overrides --skip-confirm option") do
          configuration.pretend = true
        end
        opts.on("--disable-color", "Disable output colors") do
          configuration.disable_color = true
        end
        opts.on("--logfile LOGFILE", "Logfile destination. Use - for STDOUT") do |logfile|
          if logfile == "-"
            configuration.logfile = STDOUT
          else
            configuration.logfile = logfile
          end
        end
        opts.on("--version", "Print version and exit") do
          puts(GitlabBranchRename::VERSION)
          exit 0
        end
        opts.separator ""
        opts.separator "Visit https://gitlab.com/apfritts/gitlab-branch-rename for more details."
      end

      begin
        optparse.parse!
        raise OptionParser::MissingArgument.new("old branch name") if configuration.branch_to_rename.nil?
        raise OptionParser::MissingArgument.new("new branch name") if configuration.new_branch_name.nil?
      rescue OptionParser::InvalidOption, OptionParser::MissingArgument
        puts $!.to_s
        puts optparse
        exit 1
      end

      configuration
    end
  end
end