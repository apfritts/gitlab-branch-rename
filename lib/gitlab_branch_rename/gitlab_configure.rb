require "gitlab"

module GitlabBranchRename
  class GitlabConfigure
    def self.configure(configuration)
      Gitlab.endpoint = configuration.endpoint
      Gitlab.private_token = configuration.token

      #Gitlab.http_proxy("proxyhost", 8888)
      #Gitlab.http_proxy("proxyhost", 8888, "proxyuser", "strongpasswordhere")
      #ENV["GITLAB_API_HTTPARTY_OPTIONS"] = "{read_timeout: 60}"
    end
  end
end